import "jquery-mask-plugin";

export function styledInputPhone() {
  $('input[type="tel"]').mask("+7(000)000-00-00", {
    placeholder: "+7(999)999-99-99",
    clearIfNotMatch: true
  });
}
// export function styledInputNumber() {
//   $('input[type="number"]').mask('0#');
// }

export function styledInput() {
  $(
    'input[type="text"],input[type="number"],input[type="email"],input[type="password"],input[type="tel"],textarea'
  ).on("input", function(e) {
    if (e.target.value) {
      $(this).addClass("not-empty");
    } else {
      $(this).removeClass("not-empty");
    }
  });
}
styledInput();

styledInputPhone();
// styledInputNumber();
